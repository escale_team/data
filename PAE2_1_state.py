class Data: pass
data=Data()
data.imageNames=[u'./PAE2_1_Cam1-000100.bmp', u'./PAE2_1_Cam1-000110.bmp', u'./PAE2_1_Cam2-000100.bmp', u'./PAE2_1_Cam2-000110.bmp']
data.PoVFiles=[u'./PAE2_2_1-1.pdv', u'./PAE2_2_1-2.pdv']
data.parameters={'erode': 0, 'rDefStep': True, 'iterationsX': 2, 'epipolar': True, 'histogramValueMax': 1.0, 'minTexture': 40, 'levels': 5, 'useInitPolynome': True, 'iterations': 5, 'erodeFinal': 0, 'viewInterpolated': -10, 'vSize': 2000, 'radiusV': 15, 'radiusH': 15, 'saveRectified': True, 'minWindowsOverlap': 20, 'symmetric': False, 'maxDisplacement': 20, 'minCorrelation': 40, 'radiusStrain': 15, 'vSizeAuto': True}
data.FEDICparameters={'stagnation': 1e-07, 'integrationMethod': 2, 'residualMax': 0.0001, 'linearSolver': 'mumps', 'coarsening': -20, 'autoCoarseningFactor': 3.0, 'iterMax': 20, 'method': 0}
data.M1=None
data.M2=None
data.isPolOnRectified=None
