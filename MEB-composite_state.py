class Data: pass
data=Data()
data.imageNames=[u'./MEB-composite-0N.png', u'./MEB-composite-1975N.png']
data.PoVFiles=['/tmp/escalePoV-53qWxz.pdv', '/tmp/escalePoV-53qWxz.pdv']
data.parameters={'erode': 0, 'rDefStep': True, 'iterationsX': 2, 'epipolar': True, 'histogramValueMax': 1.0, 'minTexture': 40, 'levels': 5, 'useInitPolynome': True, 'iterations': 30, 'erodeFinal': 0, 'viewInterpolated': -10, 'vSize': 2000, 'radiusV': 25, 'radiusH': 25, 'saveRectified': True, 'minWindowsOverlap': 20, 'symmetric': False, 'maxDisplacement': 20, 'minCorrelation': 40, 'radiusStrain': 25, 'vSizeAuto': True}
data.FEDICparameters={'stagnation': 1e-07, 'integrationMethod': 2, 'residualMax': 0.0001, 'linearSolver': 'mumps', 'coarsening': -20, 'autoCoarseningFactor': 3.0, 'iterMax': 20, 'method': 0}
data.M1=None
data.M2=None
data.isPolOnRectified=None
